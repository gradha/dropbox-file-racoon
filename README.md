# Dropbox file racoon

## Description.

This is an unfinished search helper for [Dropbox](https://www.dropbox.com)
(full context in the blog post [Machine learning is killing Dropbox
search](https://gradha.github.io/articles/2019/05/machine-learning-is-killing-dropbox-search.html).
It accesses your account file creation/deletion history and creates a local
[SQLite database](https://sqlite.org/). This first database creation can take a
long time depending on your history activity. Once downloaded, you can search
this database from the command line to learn what filenames have been *seen* by
Dropbox.

![Muahahahaha](https://gitlab.com/uploads/-/system/project/avatar/12165279/muahahaha.jpg)


## Requirements

The project is implemented in [Kotlin](https://kotlinlang.org) and uses
[Gradle](https://gradle.org) and [SQLite JDBC Driver](SQLite JDBC Driver).

Since it is unfinished, it does **not** work for the general public despite the
source code being available. If you want to make it work, you first need to go
to [Dropbox's App Console](https://www.dropbox.com/developers/apps) and create
a project using your Dropbox account. Once you have created the project, go to
its settings and click the [Generate access token
button](https://blogs.dropbox.com/developers/2014/05/generate-an-access-token-for-your-own-account/)
to generate your own personal use token. You can then use this token as a
parameter to the program to access your Dropbox file history.

If this program was meant for public use, it would instead use the standard
OAuth authorization flow to acquire tokens for each user. Who knows, maybe I'll
do that some day if I get bored enough…


## Command line usage

To compile and build a JAR of the program, run the following command:

	$ ./gradlew buildJar

This command will generate the `app/build/libs/dropbox-file-racoon-0.1.jar`
file, which you can run directly like this:

	$ java -jar app/build/libs/dropbox-file-racoon-0.1.jar

Running the program will give you the basic command line help. The first time
you need to run the fetch parameter to retrieve your Dropbox history. Following
the instructions above, get your Dropbox developer token and pass it on the
command line with the `-f=xxxxxxxxx` parameter. The fetch command is used later
to update your Dropbox history and has to be run manually, but the token is
stored in the local database so you can pass `-f` alone in future invocations.

Use the `-s` parameter to search for something in your history:

	$ java -jar app/build/libs/dropbox-file-racoon-0.1.jar -s tumblr_
	Searching for SQLite term tumblr\_
	Got 21075 matches for 'tumblr_'.
	x /01 tumblr_mbzrsm9SDl1qbogk6o1_1280-02 tumblr_mbzrsm9SDl1qbogk6o2_1280.jpg
	…

The first column shows an `x` for deleted files and `v` for *still alive* files
(as of the last history fetch), the second column is the last known file path.


## Creating a standalone binary

If you prefer creating a *fake* binary, you can run the [dist.sh
script](https://gitlab.com/gradha/dropbox-file-racoon/blob/master/dist.sh),
which will concatenate the
[src/exestub.sh](https://gitlab.com/gradha/dropbox-file-racoon/blob/master/src/exestub.sh)
as a header of the JAR, and thus spare you the typing of the tiresome `java
-jar foo` command copying it to `~/bin/dropbox-file-racoon.jexe`.  The script
is unlikely to work for you directly, but you can read it and run the commands
as you want to put it somewhere useful (unlikely to work on Windows).
