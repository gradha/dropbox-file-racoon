#!/bin/sh

TARGET=~/bin/dropbox-file-racoon.jexe

./gradlew buildJar && \
	cat extra/exestub.sh app/build/libs/dropbox-file-racoon-0.1.jar > "$TARGET" && \
	chmod +x "$TARGET" && \
	echo "Installed as $TARGET"
