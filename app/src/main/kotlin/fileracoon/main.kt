package fileracoon

import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.RateLimitException
import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.v2.files.*
import fileracoon.EntryStateKind.*


private fun getNextBatch(state: State): ListFolderResult {
	var result: ListFolderResult? = null
	while (null == result) {
		try {
			result = rawGetNextBatch(state)
		} catch (e: RateLimitException) {
			var remaining = e.backoffMillis
			println("Rate limit, waiting ${remaining}ms")
			try {
				Thread.sleep(remaining)
			} catch (e: InterruptedException) {
				println("Couldn't sleep!")
			}
		}
	}

	return result
}


private fun rawGetNextBatch(state: State): ListFolderResult {
	if (state.db.lastCursor.isEmpty()) {
		return state.client!!.files().listFolderBuilder("")
			.withIncludeDeleted(true).withRecursive(true).start()
	} else {
		return state.client!!.files().listFolderContinue(state.db.lastCursor)
	}
}


class State(val db: Database) {
	var client: DbxClientV2? = null
}

private fun saveEntry(state: State, data: Metadata) {
	val kind = when (data) {
		is DeletedMetadata -> Deleted
		is FileMetadata -> File
		is FolderMetadata -> Folder
		else -> Unknown
	}

	if (Unknown == kind) {
		println("Unexpected entry to save $data")
		System.exit(-1)
	}

	state.db.addEntry(kind, data.name, data.pathLower, data.pathDisplay)
}

private fun fetchHistory(commandlineToken: String?) {
	val db = Database.open() ?: return println("Couldn't open database")
	val state = State(db)
	val token = commandlineToken ?: state.db.lastToken

	if (token.isEmpty()) {
		println("Pass a token to the fetch argument, I don't have any cached.")
		return
	}

	// Create Dropbox client
	val config = DbxRequestConfig("FileRacoon/0.1")
	state.client = DbxClientV2(config, token)
	val account = state.client!!.users().currentAccount
	println("Using auth token for racoon ${account.name.displayName}")
	state.db.lastToken = token

	var folderResult = getNextBatch(state)
	while (folderResult.entries.size > 0 || folderResult.hasMore) {
		folderResult.entries.forEach { entry -> saveEntry(state, entry) }
		println("Got ${folderResult.entries.size} results in page")
		db.lastCursor = folderResult.cursor

		if (folderResult.hasMore) {
			folderResult = getNextBatch(state)
		} else {
			break
		}
	}

	println("Finished with last cursor ${db.lastCursor}")
}

private fun searchHistory(text: String) {
	val db = Database.open() ?: return println("Couldn't open database")

	val searchResults = db.searchNames(text)
	println("Got ${searchResults.size} matches for '${text}'.")
	for (search in searchResults.sortedBy { it.displayName }) {
		val x = if (search.isDeleted) "x" else "v"
		println("$x ${search.displayName}")
	}
	println("Got ${searchResults.size} matches.")
}

fun main(args: Array<String>) {

	val command = parseArgs(args)
	when (command) {
		is CommandExit -> System.exit(command.errorCode)
		is CommandSearch -> searchHistory(command.text)
		is CommandFetch -> fetchHistory(command.token)
	}.safe
}
