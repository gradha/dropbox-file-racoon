package fileracoon

fun <R> tryNull(block: () -> R): R? {
	try {
		return block()
	} catch (e: Exception) {
		println("Exception ${e.message}")

		return null
	}
}

/**
 * Trick to force when checking all branches.
 * See https://stackoverflow.com/a/44383076/172690.
 */
val Any?.safe get() = Unit
