package fileracoon

import org.realityforge.getopt4j.CLArgsParser
import org.realityforge.getopt4j.CLOption
import org.realityforge.getopt4j.CLOptionDescriptor
import org.realityforge.getopt4j.CLOptionDescriptor.*
import org.realityforge.getopt4j.CLUtil


const val OPTION_HELP = 'h'.toInt()
const val OPTION_VERSION = 'v'.toInt()
const val OPTION_FETCH = 'f'.toInt()
const val OPTION_SEARCH = 's'.toInt()

sealed class Command
class CommandExit(val errorCode: Int) : Command()
class CommandSearch(val text: String) : Command()
class CommandFetch(val token: String?) : Command()


val options = arrayOf(
	CLOptionDescriptor(
		"help", ARGUMENT_DISALLOWED, OPTION_HELP, "print this help and exit"),
	CLOptionDescriptor(
		"version", ARGUMENT_DISALLOWED, OPTION_VERSION,
		"print version and exit"),
	CLOptionDescriptor(
		"fetch", ARGUMENT_OPTIONAL, OPTION_FETCH,
		"fetches dropbox history for the specified token"),
	CLOptionDescriptor(
		"search", ARGUMENT_REQUIRED, OPTION_SEARCH,
		"searches the local dropbox history")
)

fun parseArgs(args: Array<String>): Command {
	val parser = CLArgsParser(args, options)
	if (null != parser.errorString) {
		println("Error: ${parser.errorString}")
		return CommandExit(-1)
	}

	for (option in parser.arguments) {
		when (option.id) {
			OPTION_FETCH -> return CommandFetch(option.argument)
			OPTION_SEARCH -> return CommandSearch(option.argument)
			OPTION_HELP -> {
				printUsage()
				return CommandExit(0)
			}
			OPTION_VERSION -> {
				println("Version ??")
				return CommandExit(0)
			}
			CLOption.TEXT_ARGUMENT -> {
				println("Unknown arg: " + option.argument);
				printUsage()
				return CommandExit(1)
			}
		}
	}

	println("No arguments specified")
	printUsage()
	return CommandExit(0)
}

private fun printUsage() {
	val lineSeparator = System.getProperty("line.separator")

	val msg = StringBuilder()

	msg.append(lineSeparator)
	msg.append("Command-line arg parser demo")
	msg.append(lineSeparator)
	msg.append("Usage: program ")
	msg.append(lineSeparator)
	msg.append(lineSeparator)
	msg.append("Options: ")
	msg.append(lineSeparator)

	/**
	 * Notice that the next line uses CLUtil.describeOptions to generate the
	 * list of descriptions for each option
	 */
	msg.append(CLUtil.describeOptions(options).toString())
	println(msg)
}
