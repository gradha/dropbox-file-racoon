package fileracoon

import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement

private const val JDBC_PREFIX = "jdbc:sqlite:"


enum class EntryStateKind(val value: Int) {
	Unknown(0), Deleted(1), File(2), Folder(3)
}

typealias StatementCache = HashMap<String, PreparedStatement>

class Database : AutoCloseable {
	private val conn: Connection
	private var _lastCursor: String = ""
	private var _lastToken: String = ""
	private val statements: StatementCache = HashMap(10)

	companion object {
		fun open(): Database? = tryNull { Database() }
	}

	init {
		val configDir = File(System.getProperty("user.home") + "/.config/")
		configDir.mkdir()
		val dbPath = File(configDir, "dropbox-file-racoon.sqlite3")

		conn = DriverManager.getConnection(
			"${JDBC_PREFIX}${dbPath.canonicalPath}")

		migrateDb(conn)
	}

	override fun close() {
		try {
			conn.close()
		} finally {
		}
	}

	var lastCursor: String
		get() {
			if (_lastCursor.isNotEmpty()) {
				return _lastCursor
			}

			_lastCursor = fetchLastCursor(conn)
			return _lastCursor
		}
		set(value) {
			_lastCursor = value
			saveLastCursor(conn, statements, value)
		}

	var lastToken: String
		get() {
			if (_lastToken.isNotEmpty()) {
				return _lastToken
			}

			_lastToken = fetchLastToken(conn)
			return _lastToken
		}
		set(value) {
			_lastToken = value
			saveLastToken(conn, statements, value)
		}

	/**
	 * Adds an entry to the database.
	 */
	fun addEntry(state: EntryStateKind,
		name: String, path: String, display: String)
		= addEntry(conn, statements, state, name, path, display)

	/**
	 * Searches for the literal in the database.
	 *
	 * Returns a list of all the paths matching the text in the name as a lower case string.
	 */
	fun searchNames(text: String): List<SearchResult>
		= searchNames(conn, statements, text)
}

private fun prepareStatement(conn: Connection, cache: StatementCache,
	sql: String): PreparedStatement {

	var result = cache.get(sql)
	if (null == result) {
		result = conn.prepareStatement(sql)
		cache.put(sql, result)
	}

	return result!!
}

class SearchResult(val displayName: String, val isDeleted: Boolean)

private const val searchNamesSql = """
	SELECT path_display, state
	FROM paths
	WHERE lower_name LIKE ? ESCAPE '\'
	"""

private fun searchNames(conn: Connection, cache: StatementCache,
	text: String): List<SearchResult> {

	val s = prepareStatement(conn, cache, searchNamesSql)

	@Suppress("NAME_SHADOWING")
	var text = text.toLowerCase()
	text = text.replace("""\""", """\\""")
	text = text.replace("""%""", """\%""")
	text = text.replace("""_""", """\_""")
	text = text.replace("""*""", """%""")
	text = text.replace("""?""", """_""")
	s.setString(1, "%$text%")
	println("Searching for SQLite term $text")

	val ret = s.execute()
	assert(ret)
	val result = ArrayList<SearchResult>()
	val rs = s.resultSet
	while (rs.next()) {
		val path = rs.getString(1)
		val state = rs.getInt(2)
		result.add(SearchResult(path, state == EntryStateKind.Deleted.value))
	}

	return result
}


private const val addEntryUpdateSql = """
	UPDATE paths
	SET state = ?, path_display = ?
	WHERE lower_dir = ? AND lower_name = ?
	"""

private const val addEntryInsertSql = """
	INSERT INTO paths (state, lower_dir, lower_name, path_display)
	VALUES (?, ?, ?, ?)
	"""

private fun addEntry(conn: Connection, cache: StatementCache,
	state: EntryStateKind, name: String, path: String, display: String) {

	val pathFile = File(path)
	assert(pathFile.name == name)
	val dir = pathFile.parent ?: ""
	var s = prepareStatement(conn, cache, addEntryUpdateSql)

	s.setInt(1, state.value)
	s.setString(2, display)
	s.setString(3, dir)
	s.setString(4, name)

	if (s.executeUpdate() > 0) {
		return
	}

	// The update failed, which means we need to insert a new record.
	s = prepareStatement(conn, cache, addEntryInsertSql)

	s.setInt(1, state.value)
	s.setString(2, dir)
	s.setString(3, name)
	s.setString(4, display)

	val ret = s.execute()
	assert(ret)
	assert(s.updateCount > 0)
}

/**
 * @return The last stored token or the empty string.
 */
private fun fetchLastToken(conn: Connection) = fetchString(conn, 2) ?: ""

/**
 * @return The last stored cursor or the empty string.
 */
private fun fetchLastCursor(conn: Connection) = fetchString(conn, 1) ?: ""

/**
 * @return Whatever value is stored at the specified string_value row or null.
 */
private fun fetchString(conn: Connection, row: Int): String? {
	conn.createStatement().use { statement ->
		statement.executeQuery(
			"SELECT string_value FROM global_state WHERE id = $row"
		).use { rs ->
			return if (rs.isClosed) null else rs.getString(1)
		}
	}
}


private fun saveLastToken(conn: Connection, cache: StatementCache,
	value: String) = saveString(conn, cache, value, 2)

private fun saveLastCursor(conn: Connection, cache: StatementCache,
	value: String) = saveString(conn, cache, value, 1)

private const val saveStringUpdateSql
	= """UPDATE global_state SET string_value = ? WHERE id = ?"""

private const val saveStringInsertSql = """
	INSERT INTO global_state (id, string_value, int_value)
	VALUES (?, ?, NULL)"""

private fun saveString(conn: Connection, cache: StatementCache,
	value: String, row: Int) {

	var s = prepareStatement(conn, cache, saveStringUpdateSql)
	s.setString(1, value)
	s.setInt(2, row)

	if (s.executeUpdate() > 0) {
		return
	}

	s = prepareStatement(conn, cache, saveStringInsertSql)
	s.setInt(1, row)
	s.setString(2, value)

	val ret = s.execute()
	assert(ret)
	assert(s.updateCount > 0)
}

private fun migrateDb(conn: Connection) {
	val statement = conn.createStatement()
	statement.queryTimeout = 30

	statement.executeUpdate("""CREATE TABLE IF NOT EXISTS paths (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		state INTEGER NOT NULL,
		lower_dir TEXT NOT NULL,
		lower_name TEXT NOT NULL,
		path_display TEXT NOT NULL,
		CONSTRAINT path_constraint UNIQUE (lower_dir, lower_name),
		CONSTRAINT state_constraint CHECK (state IN (1, 2, 3))
		)""")

	statement.executeUpdate("""CREATE TABLE IF NOT EXISTS global_state (
		id INTEGER PRIMARY KEY,
		string_value TEXT NULL,
		int_value INTEGER NULL
		)""")
}
